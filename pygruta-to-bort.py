#!/usr/bin/python3

import sys, pygruta, re

# dumps a topic as a bort file
# usage: pygruta {gruta src} {topic}

gruta = pygruta.open(sys.argv[1])
topic = sys.argv[2]

for s in gruta.story_set(topics=topic):
    if s[0] != topic:
        continue

    story = gruta.story(s[0], s[1])

    url = ""
    body = story.get("body")

    # strip grutatxt noise
    body = body.replace("<!-- grutatxt 2.20 -->", "")

    # find the url
    x = re.search("links?://[^<]+", body)

    try:
        url = "http" + x.group(0)[4:]

        # delete this from the body
        body = body.replace(x.group(0), "")

    except:
        pass

    if url == "":
        x = re.search("https?://[^\"]+", body)

        try:
            url = x.group(0)
        except:
            pass

    # more cleaning
    body = body.replace("<p></p>", "")

    print("url: " + url)
    print("title: " + story.get("title"))
    print("date: " + story.get("date"))
    print("tags: " + ",".join(story.get("tags")))
    print("x-pygruta: %s/%s" % (s[0], s[1]))
    print()
    print(body.strip())
    print("%%%")
