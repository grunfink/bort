# bort

A bookmark manager

## Features

- Maintains URLs
- Checks link validity and updates the URL if possible
- Generates a simple, keyword-organized HTML file

## Installation

Just run `make install` as root. You'll need the Python3 external package `urllib3`.

## Usage

- `add {file.bort} {url}` adds a new link to the `.bort` file. It's created if it doesn't exist.
- `create {file.bort} {file.html}` creates an HTML file with the all the links organized by keyword.
- `check {file.bort}` checks all links in the file, updating their URLs if the site says so and marking those that fail.
- `check-bad {file.bort}` checks all links that are marked as bad (i.e. returned an HTTP status different than 200 when checked).

## Example

Start adding a link by typing:

```
bort add bookmarks.bort https://tvtropes.org
```

The link is tested and `bookmarks.bort` now contains

```
url: https://tvtropes.org
title: TV Tropes
tags: 
lang: en
date: 20220902131847
status: 200

%%%
```

Edit the file and add some comma-separated tags in the appropriate field. Also, you can add some comment (HTML allowed) between the `status` line and the `%%%` mark (don't forget to leave an empty line between the *header* and the *body*. Repeat this process many times.

You can periodically check the validity (and eventually recover from redirections) by typing

```
bort check bookmarks.bort
```

The file will be updated and some messages shown.

Then create the HTML file by running

```
bort create bookmarks.bort bookmarks.html
```

## About

Of course, the name `bort` comes from [this genious joke](https://youtu.be/Au1He0_eCkw).

"Oh, come on. Bort?"

## Author and license

See the LICENSE file for details.
